# CI/CD Workshop

## Workshop Goal
The goal of this workshop is to create your own GitLab CI pipeline for a frontend project from scratch to a fully functional pipeline. No code editors are required; all the work will take place in the browser.

<img src="./docs/qrcode.png" alt="Workshop link" width="300">

### Pre-Workshop Setup
1. Register on GitLab: [https://about.gitlab.com/](https://about.gitlab.com/)
2. Fork this repository:
    - Open the link: [https://gitlab.com/Kreezag/ci-cd-masterclass/-/forks/new](https://gitlab.com/Kreezag/ci-cd-masterclass/-/forks/new)
    - Click the "Fork project" button
3. Register on Netlify: [https://www.netlify.com/](https://www.netlify.com/) (You can use the GitLab account you just created)

At the end of the workshop, you will have your own website deployed using CI/CD on a CDN. Example: [https://ci-cd-workshop-dev.netlify.app/](https://ci-cd-workshop-dev.netlify.app/)

### Project Sections
1. **Simple build with CI:**
    - use script, before_script, image, cache

2. **Code Validation Before Build:**
    - add prepare, lint, test jobs

3. **Process Optimization:**
    - add stages, rules, needs

4. **Deployment Preparation:**
    - environments, extends, secrets

5. **Netlify Deployment:**
    - Netlify's integration, masked variables, when:manual

### Homework:
- Organize jobs into a separate folder.
- Implement a production deploy.

### Useful Resources:
- GitLab Documentation: [https://docs.gitlab.com/](https://docs.gitlab.com/)
- Netlify CLI: [https://docs.netlify.com/cli/get-started/#app](https://docs.netlify.com/cli/get-started/#app)
- Docker pipeline examples: [https://gitlab.com/pipeline-components](https://gitlab.com/pipeline-components)
- How to get Netlify Auth Token: [https://docs.netlify.com/cli/get-started/#obtain-a-token-in-the-netlify-ui](https://docs.netlify.com/cli/get-started/#obtain-a-token-in-the-netlify-ui)
- How to get Netlify Site ID: [https://docs.netlify.com/api/get-started/#get-site](https://docs.netlify.com/api/get-started/#get-site)

Feel free to explore and experiment during the workshop, and don't hesitate to ask questions. Happy coding!
